// ========== NOMOR 7 ========== //
// 7. Membuat sebuah function yang berfungsi untuk menghitung total seluruh sepatu yang terjual.
const dataPenjualanPakAldi = [
    {
      namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
      hargaSatuan: 760000,
      kategori : "Sepatu Sport",
      totalTerjual : 90,
    },
    {
      namaProduct : 'Sepatu Warrior Tristan Black Brown High',
      hargaSatuan: 960000,
      kategori : "Sepatu Sneaker",
      totalTerjual : 37,
    },
    {
      namaProduct : 'Sepatu Warrior Tristan Maroon High ',
      kategori : "Sepatu Sneaker",
      hargaSatuan: 360000,
      totalTerjual : 90,
    },
    {
      namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
      hargaSatuan: 120000,
      kategori : "Sepatu Sneaker",
      totalTerjual : 90,
    }
]


// Membuat sebuah function dengan nama "getTotalPenjualan". 
 function getTotalPenjualan (dataPenjualan){

  // Melakukan proses validasi terhadap tipe data dari parameter yang diterima.
  if (dataPenjualan === undefined) {
    return "Error: Invalid data type."
  }

    // Proses perhitungan jumlah sepatu yang telah terjual.
    const totalSeluruhSepatuTerjual = (dataPenjualan[0].totalTerjual) + (dataPenjualan[1].totalTerjual) + (dataPenjualan[2].totalTerjual) + (dataPenjualan[3].totalTerjual) 
    return totalSeluruhSepatuTerjual;
}

console.log(getTotalPenjualan(dataPenjualanPakAldi));
console.log(getTotalPenjualan());
// ========== AKHIR NOMOR 7 ========== //