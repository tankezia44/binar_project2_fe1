// ========== NOMOR 6 ========== //
// 6. Buatlah sebuah function yang berfungsi untuk mendapatkan angka terbesar kedua dari sebuah array.
// Membuat sebuah function dengan nama "getAngkaTerbesarKedua".
function getAngkaTerbesarKedua (dataNumbers){

    // Melakukan pengecekan/validasi terlebih dahulu apakah parameter sudah sesuai atau tidak.
    if(dataNumbers === undefined) {
        return "Error: Function didn't have any parameter.";
    } else if(!Array.isArray(dataNumbers)) {
        return "Error: Invalid data type.";
    } else if (Array.isArray(dataNumbers) && dataNumbers.length >=2) { //Proses untuk menampilkan angka terbesar. 
        const angkaTerbesarKedua = new Set(dataNumbers.sort((a,b)=>{return a-b}).reverse())
        return Array.from(angkaTerbesarKedua)[1]
    }
}

const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataNumbers));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
// ========== AKHIR NOMOR 6 ========== //

