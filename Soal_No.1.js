// ========== NOMOR 1 ========== //
// 1. Buatlah sebuah function dengan nama changeWord yang berfungsi untuk menggantikan sebuah kata didalam sebuah kalimat.
// Membuat sebuah function dengan nama "changeWord"
function changeWord (selectedText, changeText, text) {
    const A = text.replace(selectedText, changeText);
    return A;
}

const sentence1 = "Andini sangat mencintai kamu selamanya";
const sentence2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

console.log(changeWord('mencintai', 'membenci', sentence1));
console.log(changeWord('bromo', 'semeru', sentence2));
// ========== AKHIR NOMOR 1 ========== //






