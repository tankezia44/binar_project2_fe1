// ========== NOMOR 8 ========== //
// 8. Membuat sebuah function yang berfungsi untuk mendapatkan informasi berupa Total Keuntungan, Total Modal, Produk Buku Terlaris, Penulis Buku Terlaris dan Persentase Keuntungan.

const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

// Membuat sebuah function dengan nama "getInfoPenjualan".
function getInfoPenjualan(dataPenjualan){

  // Melakukan proses validasi terhadap tipe data dari parameter yang diterima.
  if (dataPenjualan === undefined) {
    return "Error: Invalid data type."
  }
  
  // Proses perhitungan untuk mendapatkan informasi berupa Total Keuntungan, Total Modal, Produk Buku Terlaris, Penulis Buku Terlaris dan Persentase Keuntungan.
  let totalKeuntungan = 0;
  let totalModal = 0;
  let persentaseKeuntungan = 0;
  let dataSementara = 0 ;
  let Hasil = null; 
  
  for(let i = 0; i < dataPenjualan.length; i++) {
    if(typeof dataPenjualan[i].hargaBeli === "number" || typeof dataPenjualan[i].hargajual === "number"|| typeof dataPenjualan[i].totalTerjual === "number" || typeof dataPenjualan[i].sisaStok === "number"){
      totalKeuntungan = totalKeuntungan + (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual  
    } 
  
    if(typeof dataPenjualan[i].hargaBeli === "number" || typeof dataPenjualan[i].hargajual === "number" || typeof dataPenjualan[i].totalTerjual === "number" || typeof dataPenjualan[i].sisaStok === "number" ){
      totalModal = totalModal + ( dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok ) * dataPenjualan[i].hargaBeli 
    }

    if(typeof dataPenjualan[i].hargaBeli === "number" || typeof dataPenjualan[i].hargajual === "number" || typeof dataPenjualan[i].totalTerjual === "number" || typeof dataPenjualan[i].sisaStok === "number" ){
      persentaseKeuntungan = Math.round((totalKeuntungan/totalModal) * 100) 
    }

    if(dataSementara < dataPenjualan[i].totalTerjual ){ 
      dataSementara = dataPenjualan[i].totalTerjual
      Hasil = dataPenjualan[i] 
    }     
  } 
  
  return {
    totalKeuntungan: "Rp." + totalKeuntungan.toLocaleString(), 
    totalModal: "Rp." + totalModal.toLocaleString(), 
    persentaseKeuntungan : persentaseKeuntungan.toString() + "%", 
    produkBukuTerlaris : Hasil.namaProduk, 
    penulisBukuTerlaris : Hasil.penulis,
  }     
}

console.log(getInfoPenjualan(dataPenjualanNovel));
console.log(getInfoPenjualan());
// ========== AKHIR NOMOR 8 ========== //


