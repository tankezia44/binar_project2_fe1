// ========== NOMOR 4 ========== //
// 4. Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah password yang diberikan sebagai parameter memenuhi kriteria yang telah ditentukan atau tidak.
// Membuat sebuah function dengan nama "isValidPassword".
function isValidPassword (password) {

    // Melakukan proses validasi terhadap tipe data dari parameter yang diterima.
    if(password === undefined) {
        return "Error: There is no password.";
    } else if (typeof password !== "string") {
        return "Error: A password should be in string type.";
    }

    // Proses pengecekan apakah password sudah sesuai kriteria atau tidak.
    const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test((password));
    return isValid;
}

console.log(isValidPassword('Meong2021'));
console.log(isValidPassword('meong2021'));
console.log(isValidPassword('@eong'));
console.log(isValidPassword('Meong2'));
console.log(isValidPassword(0));
console.log(isValidPassword());
// ========== AKHIR NOMOR 4 ========== //