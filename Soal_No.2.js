// ========== NOMOR 2 ========== //
// 2. Buatlah sebuah function yang berfungsi mendeteksi apakah sebuah angka termasuk angka genap atau ganjil.
// Membuat sebuah function dengan nama "checkTypeNumber" dengan percabangan if/else.
const checkTypeNumber = (givenNumber) => { 
    
    // Proses pengecekan apakah angka tersebut termasuk angka genap atau ganjil.
    if(Number.isInteger(givenNumber)) {
        return givenNumber % 2 === 0 ? "GENAP" : "GANJIL"; 
    } else {
        return givenNumber === undefined ? "Error: Bro where is the parameter?" : "Error: Invalid data type."; // Melakukan proses validasi terhadap tipe data dari parameter yang diterima.
    }
};
    
console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
// ========== AKHIR NOMOR 2 ========== //

  