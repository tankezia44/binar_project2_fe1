// ========== NOMOR 3 ========== //
// 3. Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah alamat email yang diberikan sebagai parameter, adalah alamat email yang formatnya benar atau tidak.
// Membuat sebuah function dengan nama "checkEmail".
function checkEmail (email) {

    // Melakukan proses validasi terhadap tipe data dari parameter yang diterima.
    if(email === undefined) {
        return "Error: There is no email to check.";
    } else if (typeof email !== "string") {
        return "Error: Email should be a string.";
    } else if (!/@/.test(email)) { // Proses pengecekan apakah alamat email sudah sesuai kriteria atau tidak.
        return "Error: Email should containt \"@\" character";
    } else if(/^[\w-.]+@([\w-.]+.)+\.+[\w-.]{2,3}$/.test(email)) {  
        return "VALID";
    } else {
        return "INVALID";
    }
}

console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata'));
console.log(checkEmail(3322));
console.log(checkEmail());
// ========== AKHIR NOMOR 3 ========== //