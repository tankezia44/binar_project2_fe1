// ========== NOMOR 5 ========== //
// 5. Buatlah sebuah function yang berfungsi untuk membagikan sebuah nama menjadi Nama Depan, Nama Tengah, Nama Belakang.
// Membuat sebuah function dengan nama "getSplitName".
function getSplitName (personName) {
      
    // Melakukan pengecekan/validasi terlebih dahulu apakah tipe data sudah dalam bentuk string atau tidak.
    if(typeof personName !== "string") {
        return "Error: Invalid data type, please enter your name correctly!";
    } 

    // Melakukan proses splitNama dengan percabangan if/else/if. 
    const splitNama = personName.split(" ");
    if(splitNama.length === 3) {
        return {
            firstName: splitNama[0],
            middleName: splitNama[1],
            lastName: splitNama[2],
        };
    } else if(splitNama.length === 2) {
        return {
            firstName: splitNama[0],
            middleName: null,
            lastName: splitNama[1],
        };
    } else if(splitNama.length === 1){
        return {
            firstName: splitNama[0],
            middleName: null,
            lastName: null,
        };
    } else if(splitNama.length > 3) {
        return "Error: This function is only for 3 charcters name.";
    }
}

console.log(getSplitName("Aldi Daniela Pranata"))
console.log(getSplitName("Dwi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))
// ========== AKHIR NOMOR 5 ========== //